## Virtualenv

Build virtualenv:
```sh
$ virtualenv random_forest
```
Activate on Windows:
```sh
$ \random_forest\Scripts\activate
```
Deactivate:
```sh
$ deactivate
```
Create requirements.txt:
```sh
$ pip freeze > requirements.txt
```
Install required packages:
```sh
$ pip install -r requirements.txt
```
---
